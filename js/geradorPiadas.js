const btEnviar = document.querySelector("#idBtEnviarPalavra");
const colunaPiadas = document.querySelector("#idPiadas");

var campoPalavra = document.querySelector("#idPalavra");

var piadas = [];
var alertsColor = ["alert-primary", "alert-success", "alert-danger", "alert-warning", "alert-info", "alert-light", "alert-dark"]
var oldColor = "";

btEnviar.addEventListener("click", function (event) {
    event.preventDefault();

    let palavra = campoPalavra.value;

    if (palavra == null || palavra.trim() == "") {
        alert("Digite uma palavra!");
        return;
    }

    buscarPiada(palavra, adicionarVetorPiadas, montaPiada);

})


//Adicionando piada ao vetor
function adicionarVetorPiadas(piada) {
    piadas.push(piada);
    console.log(piadas)
}

//Cria uma caixa contendo uma piada
function montaPiada(piada) {
    let alertJoke = document.createElement("div")
    alertJoke.className = "fade show alert-dismissible"
    alertJoke.classList.add("alert")
   
    let textColor
    do {
        textColor = alertsColor[aleatorizar(alertsColor.length)];
    } while (oldColor != "" && textColor === oldColor); 
    oldColor = textColor;

    alertJoke.classList.add(textColor);
    alertJoke.classList.add("shadow-sm")
    alertJoke.role = "alert";
    alertJoke.textContent = piada
    colunaPiadas.appendChild(alertJoke);

    var closeBt = document.createElement("button");
    closeBt.type = "button"
    closeBt.classList.add("btn-close")
    closeBt.setAttribute("data-bs-dismiss","alert")
    closeBt.setAttribute("aria-label","Close")
    alertJoke.appendChild(closeBt);

}

function buscarPiada(palavra, cbAdicionarVetorPiadas, cbMontaPiada) {
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "https://icanhazdadjoke.com/search?term=" + palavra);
    xhr.setRequestHeader('Accept', 'application/json');

    xhr.addEventListener("load", function () {

        var resposta = JSON.parse(xhr.responseText);

        console.log("Palavra digitada: " + palavra);
        console.log(resposta.results);

        if (resposta.results.length > 0) {
            let piada = resposta.results[aleatorizar(resposta.results.length)].joke;
            console.log("Piada: " + piada)
            let piadaCapturada = piada;
            cbAdicionarVetorPiadas(piadaCapturada);
            cbMontaPiada(piadaCapturada);
        } else {
            alert("Nenhuma piada foi encontrada com essa palavra. :(\nTente com outra!");
        }
    });

    xhr.send();
}

function aleatorizar(num) {
    return Math.floor(Math.random() * num);
}
